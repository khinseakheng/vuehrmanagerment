export class NewEmployeeRequiredField{
    firstName = false as boolean;
    lastName = false as boolean;
    joinDate = false as boolean;
    email = false as boolean;
    phoneNumber = false as boolean;
    address = false as boolean;
    basicSalary = false as boolean;
    isNotMacthRequirement() : boolean{
        return this.firstName || this.lastName || this.joinDate || this.email || this.phoneNumber || this.address || this.basicSalary;
    }
}