export class EmployeeDto {
    constructor(augment?: EmployeeDto ) {
      Object.assign(this, augment)
    }
    id = '';
    joinDate =  new Date().toLocaleDateString("en-CA") as string;
    currency =  "USD" as string;
    basicSalary =  500 as number;
    gender =  "Male" as string;
    isActive =  true as boolean;
    firstName =  "" as string;
    lastName =  "" as string;
    email =  "" as string;
    phoneNumber =  "" as string;
    address =  "" as string;
    isEditing =  false as boolean;
    salary () : string {
      return this.basicSalary + ' ' +this.currency;
    }
    fullname() : string{
      return this.lastName + ' ' + this.firstName;
    }
    status() : string {
      return this.isActive ? '<span style="color:green">Active</span>' : '<span style="color:red">Inactive</span>';
    }
  }