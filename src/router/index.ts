import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import EmployeeList from '../views/EmployeeList.vue'
import NewEmployee from '../views/NewEmployee.vue'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'EmployeeList',
    component: EmployeeList
  },
  {
    path: '/new-employee',
    name: 'NewEmployee',
    component : NewEmployee
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
