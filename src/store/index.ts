import { createStore } from 'vuex'
import { EmployeeDto } from '@/Dtos'
const localStorage = window.localStorage.getItem('employees');
export default createStore({
  state: {
    employees: localStorage == null || localStorage == '' ? [] : JSON.parse(localStorage) as EmployeeDto[]
  },
  mutations: {
    addNewEmployee(state, dataModel: EmployeeDto): void {
      state.employees.push(dataModel);
      window.localStorage.setItem('employees', JSON.stringify(state.employees));
    },
    deleteOrUpdateEmployee(state, newData: EmployeeDto[]): void {
      state.employees = newData;
      window.localStorage.setItem('employees', JSON.stringify(state.employees));
    }
  },
  actions: {},
  modules: {},
  getters: {
    employeeCount(state): number {
      return state.employees.length
    }
  }
})
